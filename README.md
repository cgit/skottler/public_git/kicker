## Kicker
Kicker is a kickstart utility that uses flat files to map MAC addresses
to network and host configuration.

## Setup
1. Ensure you have python 2.7 with pip installed.
2. `pip install -r requirements.txt`
3. python kicker.py
