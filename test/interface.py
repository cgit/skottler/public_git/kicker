from nose.tools import assert_equal

class Interface(object):
    @classmethod
    def setup_klass(klass):
        pass

    def test_interface_methods(self):
        interface = Interface("eth0 52:54:00:d8:97:28")
        assert_equal(interface.get_mac_address(), "52:54:00:d8:97:28")
