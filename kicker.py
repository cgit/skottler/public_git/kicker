import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.autoreload
from jinja2 import Template
from jinja2 import Environment, PackageLoader
import sys
import os

sys.path.append(os.getcwd())

from lib.interface import Interface

class KickstartHandler(tornado.web.RequestHandler):
    def get(self):
        mac0 = self.get_argument('X-Rhn-Provisioning-Mac-0')
        if mac0 is not None:
            eth0 = Interface(mac0)
            print eth0.get_interface()
            print eth0.get_mac_addr()
        else:
            pass

        self.finish()

kicker = tornado.web.Application([
    (r"/", KickstartHandler)
])

if __name__ == "__main__":
    tornado.options.parse_command_line()
    kicker.listen(9001)
    ioloop = tornado.ioloop.IOLoop.instance()
    tornado.autoreload.start(ioloop)
    ioloop.start()
