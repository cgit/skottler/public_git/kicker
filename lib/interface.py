import string

class Interface(object):
    def __init__(self, interface):
        """ Store the NIC in the form "<if name> <MAC address> """
        self.nic_info = string.split(interface, " ")

    def get_interface(self):
        return self.nic_info[0]

    def get_mac_addr(self):
        return self.nic_info[1]
